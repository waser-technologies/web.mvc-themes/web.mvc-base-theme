from cms.toolbar_base import CMSToolbar
from cms.toolbar_pool import toolbar_pool
from cms.toolbar.items import LinkItem
from cms.cms_toolbars import LANGUAGE_MENU_IDENTIFIER

from django.utils.translation import override, ugettext_lazy as _

class NewTabItem(LinkItem):
    template = "toolbar/item_new_tab_link.html"

class HelpToolbarClass(CMSToolbar):

    def populate(self):
        theme_docs_url = "https://gitlab.com/waser-technologies/web.mvc-themes/web.mvc-base-theme"
        core_docs_url = "https://gitlab.com/waser-technologies/technologies/web.mvc"
        support_url = "https://waser.tech/Web.mvc/support/add/"

        help_menu = self.toolbar.get_or_create_menu('theme', _('Help'), position=-1)
        with override(self.current_lang):
            docs_menu = help_menu.get_or_create_menu(key='docs_cms_integration', verbose_name=_("Documentation"))
            docs_menu.add_item(NewTabItem(_('Theme'), theme_docs_url))
            docs_menu.add_item(NewTabItem(_('Web.mvc'), core_docs_url))
            help_menu.add_item(NewTabItem(_('Technical support'), support_url))

toolbar_pool.register(HelpToolbarClass)