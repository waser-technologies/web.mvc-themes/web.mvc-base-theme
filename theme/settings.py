from django.utils.translation import ugettext_lazy as _
import os.path

HAS_PURCHASED_THEME_LICENCE = True #True if os.environ.get('HAS_PURCHASED_THEME_LICENCE', False) == "true" else False,

DSS = {
    'settings': {
        'INSTALLED_APPS': ["theme"],
        'LOCALE_PATHS': ["theme/locale"],
        'TEMPLATES': {
            'DIRS': ["theme/templates"],
            'OPTIONS': {
                'context_processors': ['theme.context_processors.theme_licence'],
            },
        },
        'DJANGOCMS_FORMS_TEMPLATES': [
            ('djangocms_forms/default.html', 'Default'),
            ('djangocms_forms/default-invert.html', 'Clean'),
        ],
        'CKEDITOR_SETTINGS': {
            'language': '{{ language }}',
            'stylesSet': 'default:/static/js/addons/ckeditor.wysiwyg.js',
            'contentsCss': [
                '/static/djangocms_text_ckeditor/ckeditor/contents.css',
                '/static/djangocms_text_ckeditor/ckeditor/plugins/copyformatting/styles/copyformatting.css',
                'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
            ],
        },
        'CMS_TEMPLATES': [
            ## Customize this
            ('visualstudio/pages/_page.html', _('Blank page')),
            ('visualstudio/pages/_page_footer.html', _('Blank page with Footer')),
        ],
        'BLOG_IMAGE_FULL_SIZE': {'size': '370x541', 'crop': True, 'upscale': False},
        'BLOG_IMAGE_THUMBNAIL_SIZE': {'size': '340x200', 'crop': True, 'upscale': False},
        'DJANGOCMS_ICON_SETS': [
            ('fontawesome5regular', 'far', 'Font Awesome Regular'),
            ('fontawesome5solid', 'fas', 'Font Awesome 5 Solid'),
            ('fontawesome5brands', 'fab', 'Font Awesome 5 Brands'),
        ],
        'BLOG_LATEST_POSTS': 3,
        'BLOG_PLUGIN_TEMPLATE_FOLDERS': [
            ("plugins", _("Default template")),
        ],
        'NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS': [
            ("plugins", _("Default template")),
        ],
        'SECTION_DEFAULT_ATTRIUTES': {'class': 'section py-4'},
        'SECTION_INVERTED_ATTRIUTES': {'class': 'section section-inverted py-4'},
    }
}