import os
import pickle
import yaml

def module_path(module_name):
    module = __import__(module_name.split(".")[0])
    path = os.path.dirname(module.__file__)
    return path

def load_from_yaml(filepath, default={}):
    if not os.path.isfile(filepath):
        file = open(filepath, 'w')
        settings = default
        yaml.dump(settings, file, Dumper=yaml.SafeDumper)
        file.close()
    else:
        file = open(filepath, 'r')
        settings = yaml.load(file, Loader=yaml.SafeLoader)
        file.close()
    
    return settings

main_settings_path = "settings.dss"
if os.path.isfile(main_settings_path):
    print("Reading main DSS")
    print("Loading app DSS modules")
    main_file = open(main_settings_path, 'rb')
    DS_SETTINGS = pickle.load(main_file)
    main_file.close()
    dss = DS_SETTINGS.get('load_dss', [])
    print(dss)
if not os.path.isfile(main_settings_path):
    print("Creating default DSS from ./init_dss.yml")
    default_conf_file = "init_dss.yml"
    settings = {
        'settings': {
            'INSTALLED_APPS': [],
        },
        'urlpatterns': [
        ],
        'load_dss': [
            "visualstudio/settings.dss",
            "theme/settings.dss",
        ]
    }
    DS_SETTINGS = load_from_yaml(default_conf_file, default=settings)
    print("At dss file ./" + main_settings_path)
    main_file = open(main_settings_path, 'wb')
    pickle.dump(DS_SETTINGS, main_file)
    main_file.close()
    print("Added main DSS at ./" + main_settings_path)

for app in DS_SETTINGS.get('load_dss', []):
    dss_src_absolute = os.path.join(module_path(app.split("/")[0]), app.split("/")[1])
    if os.path.isfile(dss_src_absolute):
        print("Found app DSS for module " + app.split("/")[0])
    if not os.path.isfile(dss_src_absolute):
        app_settings = __import__(app.split("/")[0] + "." + app.split("/")[1].split(".")[0])
        app_dss_file = open(dss_src_absolute, 'wb')
        pickle.dump(app_settings.settings.DSS, app_dss_file)
        app_dss_file.close()
        print("Added app DSS at module " + app.split("/")[0])
